const bitcoinCore = require("bitcoin-core");

const client = new bitcoinCore({
  network: "regtest",
  // host: "192.168.101.215",
  host: "127.0.0.1",
  // port: 18443,
  port: 18332,
  username: "bitcoinrpc",
  password: "rpcpass"
});

function executeCommand(m, p, callback){
  client.command([{ method: m, parameters: p }], (error, help) => {
    if (error)
      callback(error);
    else
      callback(help);
  });
}


module.exports = {
  getInfo: function(callback) {
    client.command("getblockchaininfo", (error, help) => {
      callback(help);
    });
  },

  getBlockHash: function(height, callback) {
    executeCommand('getblockhash', [ parseInt(height) ], function(result) {
      callback(result);
    });
  },

  getBlock: function(hash, callback) {
    executeCommand('getblock', [ hash ], function(result) {
      callback(result);
    });
  },

  getBlockByHeight: function(height, callback) {
    executeCommand('getblockhash', [ parseInt(height) ], function(result) {
      executeCommand('getblock', result, function(result) {
        callback(result);
      });
    });
  },

  getRawTransaction: function(txid, callback) {
    executeCommand('getrawtransaction', [ txid, true ], (result) => {
      callback(result);
    });
  },
}
